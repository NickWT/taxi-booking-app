//Create new xhr request
var xhr = createRequest();
//Run this function when booking form button is clicked
function booking(dataSource, ID, aName, aPhone, aDate, aTime, aUnit, aStreet_num, aStreet_name, aDropoff) {
  if (xhr) {
    var obj = document.getElementById(ID);
    var requestbody = "name=" + encodeURIComponent(aName)
      + "&phone=" + encodeURIComponent(aPhone)
      + "&date=" + encodeURIComponent(aDate)
      + "&time=" + encodeURIComponent(aTime)
      + "&unit=" + encodeURIComponent(aUnit)
      + "&street_num=" + encodeURIComponent(aStreet_num)
      + "&street_name=" + encodeURIComponent(aStreet_name)
      + "&dropoff=" + encodeURIComponent(aDropoff);
    xhr.open("POST", dataSource, true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.onreadystatechange = function () {
      if (xhr.readyState == 4 && xhr.status == 200) {
        obj.innerHTML = "<br>" + xhr.responseText;
      }
    }
    xhr.send(requestbody);
  }
}