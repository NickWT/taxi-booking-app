//Create new xhr request
var xhr = createRequest();
//When Show Pick-Up Requests button is clicked showRequest function is run
function showRequest(dataSource, ID) {
  if (xhr) {
    var obj = document.getElementById(ID);
    xhr.open("POST", dataSource, true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.onreadystatechange = function () {
      if (xhr.readyState == 4 && xhr.status == 200) {
        obj.innerHTML = "<br>" + xhr.responseText;
      }
    }
    xhr.send();
  }
}
//When Assign Taxi button is pressed assignTaxi function is run
function assignTaxi(dataSource, ID, BookingNumber) {
  if (xhr) {
    var obj = document.getElementById(ID);
    var requestbody = "bookingNumber=" + encodeURIComponent(BookingNumber);
    xhr.open("POST", dataSource, true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.onreadystatechange = function () {
      if (xhr.readyState == 4 && xhr.status == 200) {
        obj.innerHTML = "<br>" + xhr.responseText;
      }
    }
    xhr.send(requestbody);
  }
}

