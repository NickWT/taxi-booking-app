<?php
// Establishes a connection to database  
$conn = mysqli_connect("host", "databse", "password");
$db_selected = mysqli_select_db($conn, "database");
//Create Booking Table in database database if not already created, with defined fields
$sqlCreateTable = "CREATE TABLE IF NOT EXISTS Booking
	(
    Customer_Name VARCHAR(45) NOT NULL,
    Customer_Phone INT(15) NOT NULL,
    Pickup_Date DateTime NOT NULL,
    Pickup_Address VARCHAR(300) NOT NULL,
    Dropoff_address VARCHAR(300) NOT NULL,
		Booking_Number INT(10) NOT NULL,
		Booking_Date DateTime NOT NULL,
		Booking_Status VARCHAR(15) NOT NULL,
		PRIMARY KEY(Booking_Number)
	)";
echo mysqli_error($conn);
//Run the query
mysqli_query($conn, $sqlCreateTable);
?>