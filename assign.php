<?php
//Save input from Client into variable
$booking_num = $_POST['bookingNumber'];
//Establish connection with the database
include('bookingdb.php');
//Query Booking table for the inputted booking number
$select = "SELECT * FROM Booking WHERE Booking_Number = '$booking_num'";
$result = @mysqli_query($conn, $select) or die ("Error " . mysqli_error($conn));
//if booking number is in the table run if statement
if (@mysqli_num_rows($result) > 0) {
    //Query the Booking table for Booking number which has a Booking status of 'assigned'
    $sql = "SELECT * FROM Booking WHERE Booking_Number = '$booking_num' AND Booking_Status = 'assigned'";
    $result = @mysqli_query($conn, $sql) or die ("Error " . mysqli_error($conn));
    //if there are rows to display run if statement
    if (@mysqli_num_rows($result)> 0) {
        echo "A taxi is already assigned to this Booking";
    } else {
        // Update the Booking status value to 'assigned'
        $update = "UPDATE Booking SET Booking_Status = 'assigned' WHERE Booking_Number = '$booking_num'";
        $result = @mysqli_query($conn, $update) or die ("Error " . mysqli_error($conn));
        //Display Confirmation Message
        echo "The booking request $booking_num has been properly assigned";
    }
} else {
  //Display error message
  echo"Booking number is not valid, please re-enter a valid Booking number.";
}
?>