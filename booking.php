<?php
// Save Input from client to variables
$name = $_POST['name'];
$phone = $_POST['phone'];
$date = $_POST['date'];
$time = $_POST['time'];
$unit = $_POST['unit'];
$street_number = $_POST['street_num'];
$street_name = $_POST['street_name'];
$dropoff = $_POST['dropoff'];
//Call date_generator function and save result to variable
$pickup_datetime = date_generator($date, $time);
//Combine $street_number and $street_name and save to variable
$pickup_address = $street_number . " " . $street_name;
//Call bookingnum_generator and save value to variable
$booking_num = bookingnum_generator();
//Save system date/time to variable in format Y-m-d H:i
$currentDateTime = date('Y-m-d H:i');
//Variable declaration
$booking_status = 'unassigned';
$correct_credentials = true;
//Checks if customer name only contains letters, and contains a value.
if (!preg_match('/[a-zA-Z]/', $name) || empty($name)) {
    echo "Name input is not valid <br>";
    $correct_credentials = false;
}
//Checks if customer phone number only contains numbers, and that the value is a least 6 numbers long, and contains a value
if (!preg_match('/[0-9]{6}/', $phone) || empty($phone)) {
    echo "Phone number is not valid <br>";
    $correct_credentials = false;
}
//Checks if street number only contains numbers and contains a value.
if (!preg_match('/[0-9]/', $street_number) || empty($street_number)) {
    echo "Pickup street number is not valid <br>";
    $correct_credentials = false;
}
//Checks if street name only contains letters, and contains a value.
if (!preg_match('/[a-zA-Z]/', $street_name) || empty($street_name)) {
    echo "Pickup street name is incorrect <br>";
    $correct_credentials = false;
}
//Checks if dropoff only contains numbers and letters, and contains a value
if (!preg_match('/[a-zA-Z0-9]/', $dropoff) || empty($dropoff)) {
    echo "Drop off address is incorrect <br>";
    $correct_credentials = false;
}
//Checks if date only contains '-' and numbers, checks if time only contains ':' and numbers, and that both variables contain values
if (!preg_match('/-[0-9]/', $date) || !preg_match('/:[0-9]/', $time) || empty($date) || empty($time)) {
    echo "Requested pickup time and date are invalid <br>";
    $correct_credentials = false;
}
//Checks if customer pickup date/time is less than the current date/time
if ($pickup_datetime < $currentDateTime) {
    echo "Pick up date/time cannot be set in the past<br>";
    $correct_credentials = false;
}
//if customer inputs are validated run the if statement contents
if ($correct_credentials == true) {
    //establish connection to database
    include('bookingdb.php');
    //Query the table for the generated booking number
    $Booking_num_exists = "SELECT * FROM Booking WHERE Booking_Number = '$booking_num'";
    $sql = @mysqli_query($conn, $Booking_num_exists) or die("Error " . mysqli_error($conn));
    $rows = @mysqli_num_rows($sql);
    //Checks to see if generated booking number is already in the table if it is, another one is generated
    if (!empty($rows)) {
        $booking_num = bookingnum_generator();
    }
    //Insert values into Booking Table
    $sqlinsert = "INSERT INTO Booking(Booking_Number, Booking_Date, Booking_Status, Customer_Name, Customer_Phone, Pickup_Address, Pickup_Date, Dropoff_address) 
    VALUES ('$booking_num', '$currentDateTime', '$booking_status', '$name', '$phone', '$pickup_address', '$pickup_datetime', '$dropoff')";
    $query = @mysqli_query($conn, $sqlinsert) or die("Error " . mysqli_error($conn));
    //close connection to database
    mysqli_close($conn);
    //Dispaly Booking confirmation message with booking number, time, and date
    echo "Thank you! <br> Your booking number is $booking_num. You will be picked up in front of your provided
    address at $time on $date.";
}
//Function to combine inputted date and time and format as Y-m-d H:I
function date_generator($aDate, $aTime){
    $merge_datetime = strtotime($aDate . " " . $aTime);
    return (date('Y-m-d H:i', $merge_datetime));
}
//Function to generate a random number between 0 and 1000000
function bookingnum_generator()
{
    return (rand(0, 1000000));
}
?>