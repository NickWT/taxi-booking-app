/*
  Student Name: Nick Winter
  Student ID: 15918723

  Creates new xhr request for transferrring data between the web browser and server.
*/
function createRequest() {
  var xhr = false;
  if (window.XMLHttpRequest) {
    xhr = new XMLHttpRequest();
  }
  else if (window.ActiveXObject) {
    xhr = new ActiveXObject("Microsoft.XMLHTTP");
  }
  return xhr;
}