<?php
//Save the system date and time to a variable
//Save the current date time + 2 hours to a variable 
$currentDateTime = date('Y-m-d H:i:s');
$futureDateTime = date("Y-m-d H:i:s", strtotime('+2 hours'));
//Connect to Database
include('bookingdb.php');
//Select all rows with unassigned booking status's which also have a pick date/time within to hours of the current time
$select = "SELECT Booking_Number, Customer_Name, Customer_Phone, Pickup_Address, Dropoff_address, Pickup_Date FROM Booking WHERE Booking_Status = 'unassigned' AND Pickup_Date BETWEEN '$currentDateTime' AND '$futureDateTime'";
//Save the query results to a variable
$result = @mysqli_query($conn, $select) or die("Error " . mysqli_error($conn));
//Create Table and display column headings in a single row
echo "<table>
				<tr>
					<th>Booking Number</th>
					<th>Customer Name </th>
					<th>Customer Phone</th>
					<th>Pick Up Address</th>
					<th>Destination</th>
					<th>Pick Up Date</th>
        </tr>";
if ($rows = @mysqli_num_rows($result) > 0) {
  // Output data of each row in a table format
  while ($row = mysqli_fetch_array($result)) {
    echo "<tr>";
    echo "<td>" . $row['Booking_Number'] . "</td>";
    echo "<td>" . $row['Customer_Name'] . "</td>";
    echo "<td>" . $row['Customer_Phone'] . "</td>";
    echo "<td>" . $row['Pickup_Address'] . "</td>";
    echo "<td>" . $row['Dropoff_address'] . "</td>";
    echo "<td>" . $row['Pickup_Date'] . "</td>";
    echo "</tr>";
  }
} else {
  //Display error message
  echo "No requests can be found. <br><br>";
}
//Close table
echo "</table>";
//Close database Connection
mysqli_close($conn);
?>