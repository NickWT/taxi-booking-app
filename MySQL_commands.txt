Name: Nick Winter
Student ID: 15918723

Description: SQL commands to create database table

CREATE TABLE IF NOT EXISTS Booking
(
Customer_Name VARCHAR(45) NOT NULL,
Customer_Phone INT(15) NOT NULL,
Pickup_Date DateTime NOT NULL,
Pickup_Address VARCHAR(300) NOT NULL,
Dropoff_address VARCHAR(300) NOT NULL,
Booking_Number INT(10) NOT NULL,
Booking_Date DateTime NOT NULL,
Booking_Status VARCHAR(15) NOT NULL,
PRIMARY KEY(Booking_Number)
)";